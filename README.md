# README #

## Docker

```
docker build . amioranza/cv:br
docker run --rm -it -p 8000:80 -v $(pwd)/conf/cv.conf:/etc/nginx/conf.d/default.conf amioranza/cv:br
```

## Kubernetes

```
kubectl apply -f cv.yaml
```

Esse descritor irá criar um deployment de 3 replicas com o container amioranza/cv:br, um service que aponta para a porta 80 do pod e um ingress controller que irá acessar o service.