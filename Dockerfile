FROM alpine as stage
RUN mkdir /cv
WORKDIR /cv
COPY html .

FROM nginx:alpine
WORKDIR /var/www/html
COPY --from=stage /cv/ .
RUN chown -R nginx:nginx .
COPY conf/cv.conf /etc/nginx/conf.d/default.conf